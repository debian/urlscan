urlscan for Debian
------------------

The urlscan source is obtained from <https://github.com/firecat53/urlscan> by
the release tag. The Debian packaging tracks the latest tarball with
pristine-tar from the above url.

The urlscan package source is maintained with dgit and on Debian salsa in
https://salsa.debian.org/debian/urlscan on Debian branches named accordingly to
DEP-14 (https://dep-team.pages.debian.net/deps/dep14/).

  * Starting from upload of version 0.9.4-1 uploads to Debian are made with
    **dgit** tool
    ** after upstream release on Github following set off commands should allow
      for source only Debian upload
      - gbp import-orig --uscan - for fetching orig tarball of the new release
        (it is using uscan)
      - do some work on the package, commit, test etc
      - dgit --gbp sbuild (or build depends on local setup)
      - dgit --gbp push-source

  * 'upstream' branch is where upstream release branch is located
  * The 'debian/*' branches are for keeping Debian package specific files (per
    suit) and merging in (with --no-ff) from 'master' branch to build packages.
    ** There are no Debian specific patches as package maintainers are also
      the upstream
  * We carry upstream tags on upstream branch prefixed with 'upstream/'
    applied by import-orig, etc ex.: upstream/1.0.1
  * We prefix our tags with 'debian/' e.g. debian/1.0.1-1 (this is usually done
    by dgit)
    ** experimental releases are prefixed 'debian/experimental'
      e.g. debian/experimental/4.0-1
    ** Debian tagging should be done after package had been successfully
      uploaded to Debian repository (to avoid having tags for debs which doesn't
      exist in the archive)

If the above explanations are not clear or need further elaboration please as
maintainer(s).

Information how to use gbp work flow following links may be useful:
https://honk.sigxcpu.org/piki/development/debian_packages_in_git/
https://wiki.debian.org/PackagingWithGit
